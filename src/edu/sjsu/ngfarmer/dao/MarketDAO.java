package edu.sjsu.ngfarmer.dao;

import edu.sjsu.ngfarmer.vo.MarketVO;

public interface MarketDAO extends BaseDAO  {
	public MarketVO[] getAllMarkets() throws Exception;
	public MarketVO getMarketById(long marketId) throws Exception;
	public void addMarket(MarketVO market) throws Exception;
	public void updateMarket(MarketVO market) throws Exception;
	public void deleteMarket(int marketId) throws Exception;

}

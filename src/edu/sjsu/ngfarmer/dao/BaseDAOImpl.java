package edu.sjsu.ngfarmer.dao;

import java.sql.Connection;
import java.sql.DriverManager;

public class BaseDAOImpl implements BaseDAO {

	private static Connection connection = null;
	public BaseDAOImpl()
	{
	
	}
	
	// You need to close the resultSet
	public void close() {
	    try {
	      if (connection != null) {
	    	  connection.close();
	      }
	    } catch (Exception e) {
	    	e.printStackTrace();
	    }
	  }

	public Connection getConnection() throws Exception {
		if(connection == null || connection.isClosed())
		{
			return getNewConnection();
		}
		return connection;
	}

	public void setConnection(Connection connection) {
		this.connection = connection;
	}
	
	private Connection getNewConnection()
	{
		try 
		{
			// This will load the MySQL driver, each DB has its own driver
			Class.forName("com.mysql.jdbc.Driver");
			// Setup the connection with the DB
			connection = DriverManager.getConnection("jdbc:mysql://aa2zoawdyvh9uy.crsn8f73vqli.us-west-2.rds.amazonaws.com:3306/ebdb?user=radukkad&password=passw0rd");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return connection;
	}

}

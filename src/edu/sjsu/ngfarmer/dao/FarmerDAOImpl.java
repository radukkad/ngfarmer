package edu.sjsu.ngfarmer.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import edu.sjsu.ngfarmer.vo.FarmerVO;

public class FarmerDAOImpl extends BaseDAOImpl implements FarmerDAO {

	private ResultSet resultSet = null;
	private PreparedStatement preparedStatement = null;
	
	@Override
	public FarmerVO getFarmerByCellNo(String inCellNo)  throws Exception {
		Statement stmt = null;
		FarmerVO farmerVO = new FarmerVO();
		try 
		{
			stmt = getConnection().createStatement();
			resultSet = stmt.executeQuery("SELECT FARMER_ID,CELLNUMBER,PASSWORD FROM farmer WHERE farmer_id != -1 and CELLNUMBER='"+ inCellNo + "'");
			while (resultSet.next()) 
			{
				
				int farmerId = resultSet.getInt("FARMER_ID");
				String cellNo = resultSet.getString("CELLNUMBER");
				String password = resultSet.getString("PASSWORD");
				
				farmerVO.setFarmerId(farmerId);
				farmerVO.setCellNo(cellNo);
				farmerVO.setPassword(password);
			}
			
		}
		catch(Exception e)
		{
			throw e;
			//e.printStackTrace();
		}
		finally
		{
			try
			{
				if (resultSet != null) {
					resultSet.close();
				}
				if (stmt != null) {
					stmt.close();
				}
				close();
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		return farmerVO;
	}

	@Override
	public FarmerVO getFarmerByFarmerId(int farmerId) throws Exception {
		FarmerVO userVO = new FarmerVO();
		Statement stmt = null;
		try 
		{
			stmt = getConnection().createStatement();
			resultSet = stmt.executeQuery("SELECT FARMER_ID,CELLNUMBER,PASSWORD FROM farmer WHERE  FARMER_ID="+ farmerId);
			while (resultSet.next()) 
			{
				
				long farmerid = resultSet.getLong("FARMER_ID");
				String cellNo = resultSet.getString("CELLNUMBER");
				String password = resultSet.getString("PASSWORD");
				
				userVO.setFarmerId(farmerId);
				userVO.setCellNo(cellNo);
				userVO.setPassword(password);
			}
			
		}
		catch(Exception e)
		{
			throw e;
			//e.printStackTrace();
		}
		finally
		{
			try
			{
				if (resultSet != null) {
					resultSet.close();
				}
				if (stmt != null) {
					stmt.close();
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		return userVO;
	}

	

	@Override
	public void addFarmer(FarmerVO farmer) throws Exception {
		try
		{
			preparedStatement = getConnection().prepareStatement("insert into farmer(cellnumber,password) values (?, ?)");
	       	preparedStatement.setString(1, farmer.getCellNo());
	       	preparedStatement.setString(2, farmer.getPassword());
	       	preparedStatement.executeUpdate();
		}
		catch(Exception e)
		{
			throw e;
			//e.printStackTrace();
		}
		finally
		{
			try
			{
				if (preparedStatement != null) {
					preparedStatement.close();
				}
				
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public void updateFarmer(FarmerVO farmer) throws Exception {
		try
		{
			preparedStatement = getConnection().prepareStatement("update farmer set cellnumber=?,password=? where farmer_id="+ farmer.getFarmerId());
	       	preparedStatement.setString(1, farmer.getCellNo());
	       	preparedStatement.setString(2, farmer.getPassword());
	       	preparedStatement.executeUpdate();
		}
		catch(Exception e)
		{
			throw e;
			//e.printStackTrace();
		}
		finally
		{
			try
			{
				if (preparedStatement != null) {
					preparedStatement.close();
				}
				
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	}


	
}

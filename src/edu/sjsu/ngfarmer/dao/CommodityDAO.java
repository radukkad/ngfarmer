package edu.sjsu.ngfarmer.dao;

import edu.sjsu.ngfarmer.vo.CommodityVO;
import edu.sjsu.ngfarmer.vo.FarmerVO;

public interface CommodityDAO extends BaseDAO  {
	public CommodityVO[] getAllCommodities()  throws Exception;
	public CommodityVO getCommodityById(long Id)  throws Exception;
	public void addSales(int marketId, int commodity, int  farmerId, String saleDate, double salesAmt) throws Exception;
	public void addCommodity(CommodityVO commodity) throws Exception;
	public void updateCommodity(CommodityVO commodity) throws Exception;
	public void deleteCommodity(int commodityId) throws Exception;
	
}

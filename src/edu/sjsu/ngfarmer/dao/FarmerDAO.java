package edu.sjsu.ngfarmer.dao;

import edu.sjsu.ngfarmer.vo.FarmerVO;

public interface FarmerDAO extends BaseDAO  {
	public FarmerVO getFarmerByCellNo(String cellNo) throws Exception;
	public FarmerVO getFarmerByFarmerId(int farmerId) throws Exception;
	public void addFarmer(FarmerVO farmer) throws Exception;
	public void updateFarmer(FarmerVO farmer) throws Exception;
}

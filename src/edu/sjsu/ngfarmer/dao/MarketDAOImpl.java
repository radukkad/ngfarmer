package edu.sjsu.ngfarmer.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import edu.sjsu.ngfarmer.vo.MarketVO;

public class MarketDAOImpl extends BaseDAOImpl implements MarketDAO {

	private ResultSet resultSet = null;
	private PreparedStatement preparedStatement = null;
	
	@Override
	public MarketVO[] getAllMarkets() throws Exception {
		Statement stmt = null;
		List<MarketVO> markets = new ArrayList();
		try 
		{
			stmt = getConnection().createStatement();
			resultSet = stmt.executeQuery("SELECT * FROM fixedmarket ");
			while (resultSet.next()) 
			{
				
				MarketVO marketVO = new MarketVO();
				
				int marketId = resultSet.getInt("MARKET_ID");
				String name = resultSet.getString("NAME");
				String district = resultSet.getString("DISTRICT");
				String postalcode = resultSet.getString("POSTALCODE");
				String state = resultSet.getString("STATE");
				String country = resultSet.getString("COUNTRY");
				
				
				marketVO.setMarketId(marketId);
				marketVO.setName(name);
				marketVO.setDistrict(district);
				marketVO.setPostalcode(postalcode);
				marketVO.setState(state);
				marketVO.setCountry(country);
				
				markets.add(marketVO);
				
			}
			
		}
		catch(Exception e)
		{
			throw e;
			//e.printStackTrace();
		}
		finally
		{
			try
			{
				if (resultSet != null) {
					resultSet.close();
				}
				if (stmt != null) {
					stmt.close();
				}
				
				close();
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		return markets.toArray(new MarketVO[markets.size()]);
	}


	

	@Override
	public MarketVO getMarketById(long marketId) throws Exception {
		Statement stmt = null;
		MarketVO market = new MarketVO();
		try 
		{
			stmt = getConnection().createStatement();
			resultSet = stmt.executeQuery("SELECT * FROM fixedmarket where market_id="+ marketId);
			while (resultSet.next()) 
			{
				
				int id = resultSet.getInt("market_id");
				String name = resultSet.getString("NAME");
				String district = resultSet.getString("DISTRICT");
				String postalcode = resultSet.getString("POSTALCODE");
				String state = resultSet.getString("STATE");
				String country = resultSet.getString("COUNTRY");
				
				market.setMarketId(id);
				market.setName(name);
				market.setDistrict(district);
				market.setPostalcode(postalcode);
				market.setState(state);
				market.setCountry(country);
				
				
			}
			
		}
		catch(Exception e)
		{
			throw e;
			//e.printStackTrace();
		}
		finally
		{
			try
			{
				if (resultSet != null) {
					resultSet.close();
				}
				if (stmt != null) {
					stmt.close();
				}
				
				close();
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		return market;
	}

	

	@Override
	public void addMarket(MarketVO market) throws Exception {
		try
		{
			preparedStatement = getConnection().prepareStatement("insert into fixedmarket(name,district,postalcode,state,country) values (?,?,?,?,?)");
	       	preparedStatement.setString(1, market.getName());
	       	preparedStatement.setString(2, market.getDistrict());
	       	preparedStatement.setString(3, market.getPostalcode());
	       	preparedStatement.setString(4, market.getState());
	       	preparedStatement.setString(5, market.getCountry());
	       	preparedStatement.executeUpdate();
		}
		catch(Exception e)
		{
			throw e;
			//e.printStackTrace();
		}
		finally
		{
			try
			{
				if (preparedStatement != null) {
					preparedStatement.close();
				}
			
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	}


	@Override
	public void updateMarket(MarketVO market) throws Exception {
		try
		{
			preparedStatement = getConnection().prepareStatement("update fixedmarket set name=?,district=?,postalcode=?,state=?,country=? where market_id="+ market.getMarketId());
	       	preparedStatement.setString(1, market.getName());
	       	preparedStatement.setString(2, market.getDistrict());
	       	preparedStatement.setString(3, market.getState());
	       	preparedStatement.setString(4, market.getCountry());
	       	preparedStatement.setString(5, market.getState());
	       	
	       	preparedStatement.executeUpdate();
		}
		catch(Exception e)
		{
			throw e;
			//e.printStackTrace();
		}
		finally
		{
			try
			{
				if (preparedStatement != null) {
					preparedStatement.close();
				}
				
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		
	}


	@Override
	public void deleteMarket(int marketId) throws Exception {
		// TODO Auto-generated method stub
		
	}
	

}

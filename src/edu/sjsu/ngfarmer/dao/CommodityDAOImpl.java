package edu.sjsu.ngfarmer.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import edu.sjsu.ngfarmer.vo.CommodityVO;
import edu.sjsu.ngfarmer.vo.FarmerVO;

public class CommodityDAOImpl extends BaseDAOImpl implements CommodityDAO {

	private ResultSet resultSet = null;
	private PreparedStatement preparedStatement = null;
	
	@Override
	public CommodityVO[] getAllCommodities() throws Exception {
		Statement stmt = null;
		List<CommodityVO> commodities = new ArrayList();
		try 
		{
			stmt = getConnection().createStatement();
			resultSet = stmt.executeQuery("SELECT * FROM commodity ");
			while (resultSet.next()) 
			{
				
				CommodityVO commodityVO = new CommodityVO();
				
				int commodityId = resultSet.getInt("COMMODITY_ID");
				String name = resultSet.getString("NAME");
				String desc = resultSet.getString("DESCRIPTION");
				
				commodityVO.setCommodityId(commodityId);
				commodityVO.setName(name);
				commodityVO.setDesc(desc);
				
				
				commodities.add(commodityVO);
				
			}
			
		}
		catch(Exception e)
		{
			throw e;
			//e.printStackTrace();
		}
		finally
		{
			try
			{
				if (resultSet != null) {
					resultSet.close();
				}
				if (stmt != null) {
					stmt.close();
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		return commodities.toArray(new CommodityVO[commodities.size()]);
	}
	
	
	@Override
	public CommodityVO getCommodityById(long Id) throws Exception {
		Statement stmt = null;
		CommodityVO commodity = new CommodityVO();
		try 
		{
			stmt = getConnection().createStatement();
			resultSet = stmt.executeQuery("SELECT * FROM commodity where commodity_id="+ Id);
			while (resultSet.next()) 
			{
				
				int commodityId = resultSet.getInt("COMMODITY_ID");
				String name = resultSet.getString("NAME");
				String desc = resultSet.getString("DESCRIPTION");
				
				commodity.setCommodityId(commodityId);
				commodity.setName(name);
				commodity.setDesc(desc);
				
			}
			
		}
		catch(Exception e)
		{
			throw e;
			//e.printStackTrace();
		}
		finally
		{
			try
			{
				if (resultSet != null) {
					resultSet.close();
				}
				if (stmt != null) {
					stmt.close();
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		return commodity;
	}
	
	@Override
	public void addSales(int marketId, int commodityId, int  farmerId, String saleDate, double salesAmt) throws Exception {
		try
		{
			String insQuery = "	INSERT INTO salesdata(commodity_id,farmer_id,market_id,sale_date,sale_price) values (?,?,?,?,?) ";
			preparedStatement = getConnection().prepareStatement(insQuery);
	       	preparedStatement.setInt(1, commodityId);
	     	preparedStatement.setInt(2, farmerId);
	     	preparedStatement.setInt(3, marketId);
	     	preparedStatement.setString(4, saleDate);
	     	preparedStatement.setDouble(5, salesAmt);
	     	
	       	preparedStatement.executeUpdate();
		}
		catch(Exception e)
		{
			throw e;
			//e.printStackTrace();
		}
		finally
		{
			try
			{
				if (preparedStatement != null) {
					preparedStatement.close();
				}
		
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	}
	

	@Override
	public void addCommodity(CommodityVO commodity) throws Exception {
		try
		{
			preparedStatement = getConnection().prepareStatement("insert into commodity(name,description) values (?, ?)");
	       	preparedStatement.setString(1, commodity.getName());
	       	preparedStatement.setString(2, commodity.getDesc());
	       	preparedStatement.executeUpdate();
		}
		catch(Exception e)
		{
			throw e;
			//e.printStackTrace();
		}
		finally
		{
			try
			{
				if (preparedStatement != null) {
					preparedStatement.close();
				}
			
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	}


	@Override
	public void updateCommodity(CommodityVO commodity) throws Exception {
		try
		{
			preparedStatement = getConnection().prepareStatement("update commodity set name=?,description=? where commodity_id="+ commodity.getCommodityId());
	       	preparedStatement.setString(1, commodity.getName());
	       	preparedStatement.setString(2, commodity.getDesc());
	       	preparedStatement.executeUpdate();
		}
		catch(Exception e)
		{
			throw e;
			//e.printStackTrace();
		}
		finally
		{
			try
			{
				if (preparedStatement != null) {
					preparedStatement.close();
				}
				
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		
	}


	@Override
	public void deleteCommodity(int commodityId) throws Exception {
		// TODO Auto-generated method stub
		
	}
	
	
}

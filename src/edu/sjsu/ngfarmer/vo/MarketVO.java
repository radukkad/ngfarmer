package edu.sjsu.ngfarmer.vo;

public class MarketVO
{
    private int marketId;
    private java.lang.String name;
    private java.lang.String district;
    private java.lang.String state;
    private java.lang.String postalcode;
    private java.lang.String country;
    
	public int getMarketId() {
		return marketId;
	}
	public void setMarketId(int marketId) {
		this.marketId = marketId;
	}
	public java.lang.String getName() {
		return name;
	}
	public void setName(java.lang.String name) {
		this.name = name;
	}
	public java.lang.String getDistrict() {
		return district;
	}
	public void setDistrict(java.lang.String district) {
		this.district = district;
	}
	public java.lang.String getState() {
		return state;
	}
	public void setState(java.lang.String state) {
		this.state = state;
	}
	public java.lang.String getPostalcode() {
		return postalcode;
	}
	public void setPostalcode(java.lang.String postalcode) {
		this.postalcode = postalcode;
	}
	public java.lang.String getCountry() {
		return country;
	}
	public void setCountry(java.lang.String country) {
		this.country = country;
	}
    
 
}

package edu.sjsu.ngfarmer.vo;

public class FarmerVO
{
    private int farmerId;
    private java.lang.String cellNo;
	private java.lang.String password;

    public int getFarmerId() {
		return farmerId;
	}
	public void setFarmerId(int farmerId) {
		this.farmerId = farmerId;
	}
	public java.lang.String getCellNo() {
		return cellNo;
	}
	public void setCellNo(java.lang.String cellNo) {
		this.cellNo = cellNo;
	}
	public java.lang.String getPassword() {
		return password;
	}
	public void setPassword(java.lang.String password) {
		this.password = password;
	}
}

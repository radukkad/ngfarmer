package edu.sjsu.ngfarmer.vo;

public class CommodityVO
{
    private int commodityId;
	private java.lang.String name;
	private java.lang.String desc;
	
    public int getCommodityId() {
		return commodityId;
	}
	public void setCommodityId(int commodityId) {
		this.commodityId = commodityId;
	}
	public java.lang.String getName() {
		return name;
	}
	public void setName(java.lang.String name) {
		this.name = name;
	}
	public java.lang.String getDesc() {
		return desc;
	}
	public void setDesc(java.lang.String desc) {
		this.desc = desc;
	}


   
}

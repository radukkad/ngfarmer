package edu.sjsu.ngfarmer.servlet;

import java.io.*;

import javax.servlet.http.*;
import javax.servlet.*;

import edu.sjsu.ngfarmer.dao.CommodityDAO;
import edu.sjsu.ngfarmer.dao.CommodityDAOImpl;
import edu.sjsu.ngfarmer.dao.FarmerDAO;
import edu.sjsu.ngfarmer.dao.FarmerDAOImpl;
import edu.sjsu.ngfarmer.vo.CommodityVO;
import edu.sjsu.ngfarmer.vo.FarmerVO;

public class AdminAddCommodityServlet extends HttpServlet {
  public void doPost (HttpServletRequest req,
                     HttpServletResponse res)
    throws ServletException, IOException
    {
	    PrintWriter out = res.getWriter();
	    
	    String name = req.getParameter("commodityname");
	    String desc = req.getParameter("desc");
	   
	    CommodityVO commodityVO = new CommodityVO();
	    commodityVO.setName(name);
	    commodityVO.setDesc(desc);
	    
	    
	    boolean success = true;
	    String errorMsg = "Commodity added successfully.";
	    try
		{
			CommodityDAO commodityDAO = new CommodityDAOImpl();
			commodityDAO.addCommodity(commodityVO);
		}
	    catch(com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException e)
	    {
	    	errorMsg = "Duplicate commodity name";
	    	e.printStackTrace(res.getWriter());
	    	success = false;
	    	
	    }
		catch(Exception e)
		{
			errorMsg = "Unable to add commodity";
	    	success = false;
			e.printStackTrace(res.getWriter());
		}
	   
	   
	    
	    req.setAttribute("errorMsg", errorMsg);
		req.getRequestDispatcher("addcommodity.jsp").forward(req, res);
		out.close();
  }
}
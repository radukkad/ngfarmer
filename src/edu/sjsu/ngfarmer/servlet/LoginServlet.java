package edu.sjsu.ngfarmer.servlet;

import java.io.*;

import javax.servlet.http.*;
import javax.servlet.*;

import edu.sjsu.ngfarmer.dao.FarmerDAO;
import edu.sjsu.ngfarmer.dao.FarmerDAOImpl;
import edu.sjsu.ngfarmer.vo.FarmerVO;

public class LoginServlet extends HttpServlet {
  public void doPost (HttpServletRequest req,
                     HttpServletResponse res)
    throws ServletException, IOException
  {
    PrintWriter out = res.getWriter();
    
    String cellno = req.getParameter("cellno");
    String inPwd = req.getParameter("password");
   
    boolean success = false;
    FarmerVO farmerVO = null;
    try
	{
		FarmerDAO farmerDAO = new FarmerDAOImpl();
    	farmerVO = farmerDAO.getFarmerByCellNo(cellno);
    	if(farmerVO.getPassword().equals(inPwd))
    	{
    		success = true;
    	}
	}
	catch(Exception e)
	{
		e.printStackTrace(res.getWriter());
	}
   
    if(success)
	{
    	HttpSession session = req.getSession();
    	session.setAttribute("cellno", cellno);
    	session.setAttribute("farmerId", farmerVO.getFarmerId());
    	res.sendRedirect("index1.jsp");
	}
	else
	{
		req.setAttribute("errorMsg", "Invalid Logon / password");
		req.getRequestDispatcher("index1.jsp").forward(req, res);

	}

    out.close();
  }
}
package edu.sjsu.ngfarmer.servlet;

import java.io.*;

import javax.servlet.http.*;
import javax.servlet.*;

import edu.sjsu.ngfarmer.dao.FarmerDAO;
import edu.sjsu.ngfarmer.dao.FarmerDAOImpl;
import edu.sjsu.ngfarmer.vo.FarmerVO;

public class UpdateUserServlet extends HttpServlet {
  public void doPost (HttpServletRequest req,
                     HttpServletResponse res)
    throws ServletException, IOException
    {
	    PrintWriter out = res.getWriter();
	    
	    String userId = req.getParameter("farmerId");
	    String cellno = req.getParameter("cellnumber");
	    String inPwd = req.getParameter("pwd");
	   
	    FarmerVO farmerVO = new FarmerVO();
	    farmerVO.setFarmerId(Integer.parseInt(userId));
	    farmerVO.setCellNo(cellno);
	    farmerVO.setPassword(inPwd);
	    
	    boolean success = true;
	    String errorMsg = "Unable to update, please retry ...";
	    try
		{
			FarmerDAO farmerDAO = new FarmerDAOImpl();
	    	farmerDAO.updateFarmer(farmerVO);
	    	errorMsg = "User information updated successfully";
		}
	    catch(com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException e)
	    {
	    	errorMsg = "Unable to update, Cell number already used";
	    	success = false;
	    }
		catch(Exception e)
		{
			success = false;
			e.printStackTrace(res.getWriter());
		}
	   
	    if(success)
	    {
	    	req.setAttribute("errorMsg", errorMsg);
			req.getRequestDispatcher("myaccount.jsp").forward(req, res);
		}
		else
		{
			req.setAttribute("errorMsg", errorMsg);
			req.getRequestDispatcher("myaccount.jsp").forward(req, res);

		}

    out.close();
  }
}
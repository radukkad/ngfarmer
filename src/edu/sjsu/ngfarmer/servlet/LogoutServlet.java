package edu.sjsu.ngfarmer.servlet;

import java.io.*;

import javax.servlet.http.*;
import javax.servlet.*;




public class LogoutServlet extends HttpServlet {
  public void doGet (HttpServletRequest req,
                     HttpServletResponse res)
    throws ServletException, IOException
  {
	
	  HttpSession session = req.getSession();
	  session.removeAttribute("cellno");
	  session.removeAttribute("farmerId");
	  res.sendRedirect("index1.jsp");
  } 
}
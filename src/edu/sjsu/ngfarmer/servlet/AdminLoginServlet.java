package edu.sjsu.ngfarmer.servlet;

import java.io.*;

import javax.servlet.http.*;
import javax.servlet.*;

import edu.sjsu.ngfarmer.dao.FarmerDAO;
import edu.sjsu.ngfarmer.dao.FarmerDAOImpl;
import edu.sjsu.ngfarmer.vo.FarmerVO;

public class AdminLoginServlet extends HttpServlet {
  public void doPost (HttpServletRequest req,
                     HttpServletResponse res)
    throws ServletException, IOException
  {
    PrintWriter out = res.getWriter();
    
    String userId = req.getParameter("userId");
    String inPwd = req.getParameter("password");
   
    boolean success = false;
    FarmerVO farmerVO = null;
    try
	{
		if(userId.equals("admin") && inPwd.equals("passw0rd"))
    	{
    		success = true;
    	}
	}
	catch(Exception e)
	{
		e.printStackTrace(res.getWriter());
	}
   
    if(success)
	{
    	HttpSession session = req.getSession();
    	session.setAttribute("admin", "true");
    	res.sendRedirect("/admin/adminhome.jsp");
	}
	else
	{
		req.setAttribute("errorMsg", "Invalid Logon / password");
		req.getRequestDispatcher("/admin/login.jsp").forward(req, res);

	}

    out.close();
  }
}
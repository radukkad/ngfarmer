package edu.sjsu.ngfarmer.servlet;

import java.io.*;

import javax.servlet.http.*;
import javax.servlet.*;

import edu.sjsu.ngfarmer.dao.FarmerDAO;
import edu.sjsu.ngfarmer.dao.FarmerDAOImpl;
import edu.sjsu.ngfarmer.vo.FarmerVO;

public class RegisterUserServlet extends HttpServlet {
  public void doPost (HttpServletRequest req,
                     HttpServletResponse res)
    throws ServletException, IOException
    {
	    PrintWriter out = res.getWriter();
	    
	    String cellno = req.getParameter("cellnumber");
	    String inPwd = req.getParameter("pwd");
	   
	    FarmerVO farmerVO = new FarmerVO();
	    farmerVO.setCellNo(cellno);
	    farmerVO.setPassword(inPwd);
	    
	    
	    boolean success = true;
	    String errorMsg = "Unable to register, please retry";
	    try
		{
			FarmerDAO farmerDAO = new FarmerDAOImpl();
	    	farmerDAO.addFarmer(farmerVO);
		}
	    catch(com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException e)
	    {
	    	errorMsg = "Cell number already registered";
	    	success = false;
	    	
	    }
		catch(Exception e)
		{
			success = false;
			e.printStackTrace(res.getWriter());
		}
	   
	    if(success)
	    {
	    	HttpSession session = req.getSession();
	    	session.setAttribute("cellno", cellno);
	    	session.setAttribute("farmerId", farmerVO.getFarmerId());
	    	res.sendRedirect("index1.jsp");
		}
		else
		{
			req.setAttribute("errorMsg", errorMsg);
			req.getRequestDispatcher("register.jsp").forward(req, res);

		}

    out.close();
  }
}
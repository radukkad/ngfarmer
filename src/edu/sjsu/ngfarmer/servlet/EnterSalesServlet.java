package edu.sjsu.ngfarmer.servlet;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.*;
import javax.servlet.*;

import edu.sjsu.ngfarmer.dao.CommodityDAO;
import edu.sjsu.ngfarmer.dao.CommodityDAOImpl;
import edu.sjsu.ngfarmer.dao.FarmerDAO;
import edu.sjsu.ngfarmer.dao.FarmerDAOImpl;
import edu.sjsu.ngfarmer.vo.FarmerVO;

public class EnterSalesServlet extends HttpServlet {
  public void doPost (HttpServletRequest req,
                     HttpServletResponse res)
    throws ServletException, IOException
    {
	    PrintWriter out = res.getWriter();
	    
	    String commodityIdStr = req.getParameter("commodity");
	    String marketIdStr = req.getParameter("marketname");
	    String farmerIdStr = req.getParameter("farmerId");
	    String salesDateStr = req.getParameter("salesdate");
		String salesAmtStr = req.getParameter("sellingprice");
		
		int commodityId = 0;
		int marketId = 0;
		int farmerId = 0;
		double salesAmt = 0.0;
		boolean inputError = false;
		try
		{
			commodityId = Integer.parseInt(commodityIdStr);
			marketId = Integer.parseInt(marketIdStr);
			farmerId = Integer.parseInt(farmerIdStr);
			salesAmt = Double.parseDouble(salesAmtStr);
			
			SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		    Date dateStr = formatter.parse(salesDateStr);
		
		}
		catch(Exception e)
		{
			inputError = true;
			e.printStackTrace(res.getWriter());
		}
		
		out.println(" commodityIdStr = "+ commodityIdStr);
		out.println(" marketIdStr = "+ marketIdStr);
		out.println(" farmerIdStr = "+ farmerIdStr);
		out.println(" salesDateStr = "+ salesDateStr);
		out.println(" salesAmtStr = "+ salesAmtStr);
		
		if(!inputError)
		{
			String errorMsg = "Unable to submit, please retry";
		    try
			{
				CommodityDAO commodity = new CommodityDAOImpl();
				commodity.addSales(marketId, commodityId, farmerId, salesDateStr, salesAmt);
				errorMsg = "Sales input successful";
			}
		 	catch(Exception e)
			{
				e.printStackTrace(res.getWriter());
			}
		    
		    out.println(" errorMsg = "+ errorMsg);
		   
			req.setAttribute("errorMsg", errorMsg);
			req.getRequestDispatcher("saleentry.jsp").forward(req, res);
		}
		else
		{
			req.setAttribute("errorMsg", "Input Error");
			req.getRequestDispatcher("saleentry.jsp").forward(req, res);
		}
		out.close();
  }
}
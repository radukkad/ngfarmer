package edu.sjsu.ngfarmer.servlet;

import java.io.*;

import javax.servlet.http.*;
import javax.servlet.*;

import edu.sjsu.ngfarmer.dao.CommodityDAO;
import edu.sjsu.ngfarmer.dao.CommodityDAOImpl;
import edu.sjsu.ngfarmer.dao.FarmerDAO;
import edu.sjsu.ngfarmer.dao.FarmerDAOImpl;
import edu.sjsu.ngfarmer.dao.MarketDAO;
import edu.sjsu.ngfarmer.dao.MarketDAOImpl;
import edu.sjsu.ngfarmer.vo.CommodityVO;
import edu.sjsu.ngfarmer.vo.FarmerVO;
import edu.sjsu.ngfarmer.vo.MarketVO;

public class AdminAddMarketServlet extends HttpServlet {
  public void doPost (HttpServletRequest req,
                     HttpServletResponse res)
    throws ServletException, IOException
    {
	    PrintWriter out = res.getWriter();
	    
	    String name = req.getParameter("marketname");
	    String dist = req.getParameter("dist");
	    String postalcode = req.getParameter("postalcode");
	    String state = req.getParameter("state");
	    String country = req.getParameter("country");
	   
	    MarketVO marketVO = new MarketVO();
	    marketVO.setName(name);
	    marketVO.setDistrict(dist);
	    marketVO.setPostalcode(postalcode);
	    marketVO.setState(state);
	    marketVO.setCountry(country);
	    
	    
	    boolean success = true;
	    String errorMsg = "Market added successfully.";
	    try
		{
			MarketDAO marketDAO = new MarketDAOImpl();
			marketDAO.addMarket(marketVO);
		}
	    catch(com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException e)
	    {
	    	errorMsg = "Duplicate market name";
	    	e.printStackTrace(res.getWriter());
	    	success = false;
	    	
	    }
		catch(Exception e)
		{
			errorMsg = "Unable to add market";
	    	success = false;
			e.printStackTrace(res.getWriter());
		}
	   
	    req.setAttribute("errorMsg", errorMsg);
		req.getRequestDispatcher("addmarket.jsp").forward(req, res);
		out.close();
  }
}
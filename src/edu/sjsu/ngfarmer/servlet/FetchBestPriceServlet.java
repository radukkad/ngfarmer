package edu.sjsu.ngfarmer.servlet;

import java.io.*;
import java.math.BigDecimal;
import java.sql.Date;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.http.*;
import javax.servlet.*;

import com.amazonaws.auth.ClasspathPropertiesFileCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ComparisonOperator;
import com.amazonaws.services.dynamodbv2.model.Condition;
import com.amazonaws.services.dynamodbv2.model.ScanRequest;
import com.amazonaws.services.dynamodbv2.model.ScanResult;
import com.mysql.jdbc.StringUtils;

import edu.sjsu.ngfarmer.dao.CommodityDAO;
import edu.sjsu.ngfarmer.dao.CommodityDAOImpl;
import edu.sjsu.ngfarmer.dao.MarketDAO;
import edu.sjsu.ngfarmer.dao.MarketDAOImpl;
import edu.sjsu.ngfarmer.vo.CommodityVO;
import edu.sjsu.ngfarmer.vo.MarketVO;

public class FetchBestPriceServlet extends HttpServlet {
	static AmazonDynamoDBClient dynamoDB;
	public void doPost (HttpServletRequest req,
                     HttpServletResponse res)
    throws ServletException, IOException
  {
    PrintWriter out = res.getWriter();
 
    dynamoDB = new AmazonDynamoDBClient(new ClasspathPropertiesFileCredentialsProvider());
    Region usWest2 = Region.getRegion(Regions.US_WEST_2);
    dynamoDB.setRegion(usWest2);

    String commodityId = req.getParameter("commodity");
    String marketId = req.getParameter("marketname");
    
    MarketVO marketVO = null;
    CommodityVO commodityVO = null;
    try
    {
    
    	MarketDAO marketDAO = new MarketDAOImpl();
    	marketVO = marketDAO.getMarketById(Long.parseLong(marketId));
    
    	CommodityDAO commodityDAO = new CommodityDAOImpl();
    	commodityVO = commodityDAO.getCommodityById(Long.parseLong(commodityId));
    
    }
    catch(Exception e)
    {
    	// req.getRequestDispatcher("index1.jsp").forward(req, res);
    	e.printStackTrace();
    }
   
    String keyStr = commodityId+"_"+ marketId;

    List<Map<String,AttributeValue>> resultList = new ArrayList();
    List outList = new ArrayList();
    try
	{
    	 String tableName = "BestMarketPrice";
    	 HashMap<String, Condition> scanFilter = new HashMap<String, Condition>();
         Condition condition = new Condition().withComparisonOperator(ComparisonOperator.EQ).withAttributeValueList(new AttributeValue(keyStr));
         scanFilter.put("CommodityID_MarketID", condition);
         ScanRequest scanRequest = new ScanRequest(tableName).withScanFilter(scanFilter);
         ScanResult scanResult = dynamoDB.scan(scanRequest);
         
         resultList = scanResult.getItems();
         out.println(">>--->"+ resultList.toString());
         Iterator<Map<String,AttributeValue>> itr = resultList.iterator();
        
         while(itr.hasNext())
         {
        	 
        	 Map<String,AttributeValue> attMap = itr.next();
        	 out.println("attMap="+ attMap);
        	 Set entrySet = attMap.entrySet();
        	 Iterator setItr = entrySet.iterator();
        	 String[] rowArray = new String[3];
     		 while(setItr.hasNext())
        	 {
        		 Entry attr = (Entry)setItr.next();
        		 out.println("entry= = "+ attr);
        		 if(attr.getKey().equals("Date"))
        		 {
        			 AttributeValue attrVal = (AttributeValue)attr.getValue();
        			 rowArray[0] =  attrVal.getS(); 
        		 }
        		 else if(attr.getKey().equals("Price"))
        		 {
        			 AttributeValue attrVal = (AttributeValue)attr.getValue();
        			 rowArray[1] = attrVal.getS();
        		 }
        		 else if(attr.getKey().equals("CellNumber"))
        		 {
        			 AttributeValue attrVal = (AttributeValue)attr.getValue();
        			 rowArray[2] = attrVal.getS();
        		 }
        	 }
     		 outList.add(rowArray);
         }
         
         out.println("result"+ outList);
         
      
    	
	}
	catch(Exception e)
	{
		e.printStackTrace(res.getWriter());
	}
    
    // get last 7 days data only
    java.util.Date maxDate = getTodaysDate();
    String maxDateStr = "";
    String cellNumber = "";
    String maxPrice = "Unavailable";
    List finalList = new ArrayList();
    List dateList = new ArrayList();
    try
    {
    	Iterator itr = outList.iterator();
    	while(itr.hasNext())
    	{
    		String[] resVal = (String[]) itr.next();
    		//if(itr.hasNext())
    		//{
    			String dateString = resVal[0];
    			DateFormat df = new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH);
    			java.util.Date resultDate =  df.parse(dateString);  
    			out.println("resultDate="+ resultDate);
    			if(resultDate.before(getTodaysDate()) && resultDate.after(getStartDate()))
    			{
    				finalList.add(resVal);
    				dateList.add(resultDate);
    				java.util.Date date = getMaxDate(dateList);
    				
    				if(date.equals(resultDate))
    				{
    					maxDate = resultDate;
    					maxPrice = resVal[1];
    					cellNumber = resVal[2];
    				}
    			}
    			else
    			{
    				out.println(" date out of range  "+ resultDate);
    			}
    		//}
    	}
    }
    catch(Exception e)
    {
    	e.printStackTrace(out);
    }
    
    
    
    try
    {
    
    	if(!maxPrice.equals("Unavailable"))
    	{
    		Object resObj = new DecimalFormat().parse(maxPrice.trim());
    		Format format = java.text.DecimalFormat.getCurrencyInstance(new Locale("en", "in"));
    		maxPrice =  format.format(resObj);
    		
    		SimpleDateFormat outDateFormat = new SimpleDateFormat("MM/dd/yyyy");
    		maxDateStr = outDateFormat.format(maxDate);
    	}
    }
    catch(Exception e)
    {
    	e.printStackTrace(out);
    }
    
    out.println("outList="+ outList);
    out.println("maxPrice="+ maxPrice);
    out.println("maxPriceDate="+ maxDate);
    
    req.setAttribute("outList", finalList);
    req.setAttribute("maxPrice", maxPrice);
    req.setAttribute("maxPriceDate", maxDateStr);
    req.setAttribute("marketVO", marketVO);
    req.setAttribute("commodityVO", commodityVO);
    
    if(!StringUtils.isEmptyOrWhitespaceOnly(cellNumber))
    {
    	 req.setAttribute("cellNumber", cellNumber);
    }
    
    
   req.getRequestDispatcher("bestpricedisplay.jsp").forward(req, res);
  
   	out.close();
  	}
	
	private java.util.Date getStartDate()
	{
	
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		Calendar c = Calendar.getInstance();
		c.setTime(new java.util.Date()); // Now use today date.
		c.add(Calendar.DATE, -7); // minus 7 days
		return c.getTime();
	}
	
	
	
	private java.util.Date getTodaysDate()
	{
	
		return new java.util.Date(); // Now use today date.
	}
	
	
	private java.util.Date getMaxDate(List<java.util.Date> listDate)
	{
		return Collections.max(listDate);
	}
	
	
}
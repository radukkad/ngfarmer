package edu.sjsu.ngfarmer.servlet;

import java.io.*;

import javax.servlet.http.*;
import javax.servlet.*;

import edu.sjsu.ngfarmer.dao.CommodityDAO;
import edu.sjsu.ngfarmer.dao.CommodityDAOImpl;
import edu.sjsu.ngfarmer.dao.FarmerDAO;
import edu.sjsu.ngfarmer.dao.FarmerDAOImpl;
import edu.sjsu.ngfarmer.vo.CommodityVO;
import edu.sjsu.ngfarmer.vo.FarmerVO;

public class AdminUpdCommodityServlet extends HttpServlet {
  public void doPost (HttpServletRequest req,
                     HttpServletResponse res)
    throws ServletException, IOException
    {
	    PrintWriter out = res.getWriter();
	    
	    String commodityIdStr = req.getParameter("commodityId");
	    String name = req.getParameter("commodityname");
	    String desc = req.getParameter("desc");
	    int commodityId = Integer.parseInt(commodityIdStr);

	    CommodityVO commodityVO = new CommodityVO();
	    commodityVO.setCommodityId(commodityId);
	    commodityVO.setName(name);
	    commodityVO.setDesc(desc);
	    
	    boolean success = true;
	    String errorMsg = "Commodity updated successfully";
	    try
		{
			CommodityDAO commodityDAO = new CommodityDAOImpl();
			commodityDAO.updateCommodity(commodityVO);
		}
	    catch(com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException e)
	    {
	    	errorMsg = "Duplicate commodity name";
	    	success = false;
	    	
	    }
		catch(Exception e)
		{
			errorMsg = "Unable to add the commodity";
			success = false;
			e.printStackTrace(res.getWriter());
		}
	   
	    req.setAttribute("errorMsg", errorMsg);
	    req.setAttribute("commodityId", commodityIdStr);
		req.getRequestDispatcher("editcommodity.jsp").forward(req, res);
		out.close();
  }
}
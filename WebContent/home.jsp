<%@page import="edu.sjsu.ngfarmer.dao.CommodityDAOImpl"%>
<%@page import="edu.sjsu.ngfarmer.dao.CommodityDAO"%>
<%@page import="edu.sjsu.ngfarmer.vo.CommodityVO"%>
<%@page import="edu.sjsu.ngfarmer.dao.MarketDAOImpl"%>
<%@page import="edu.sjsu.ngfarmer.dao.MarketDAO"%>
<%@page import="edu.sjsu.ngfarmer.vo.MarketVO"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>ngfarmer.com home</title>
</head>
<body>
<p><b><font size="16" color="#ff0000"> :: Next Generation Farmer Portal Home ::</font></b></p>
<a href="saleentry.jsp">Input new sales</a></br>
<%
Object farmerId = session.getAttribute("farmerId");
if(farmerId == null)
{
	request.getRequestDispatcher("/login.jsp").forward(request, response);
}
%>
<p><b>Find the best price today :</b></p>
<form action="FetchBestPrice" method="post">
	<table border="0" cellpadding="3" cellspacing="0" width="360">
		<tr>
			<td>Commodity</td>
			<td>
				<select name="commodity" size="1"  id="commodity" style="width:150px; border:1px solid #999999"  data-dojo-type="dijit/form/ComboBox"  >
					<option selected value="">Commodity...</option>
					<%
						CommodityDAO commodityDAO = new CommodityDAOImpl();
						CommodityVO[] commodities = commodityDAO.getAllCommodities();
						for(int i=0; i < commodities.length; i++)
						{
					%>
				
						<option value='<%=commodities[i].getCommodityId()%>'><%=commodities[i].getName()%></option>
				
					<%
					}
					%>
				</select>
			</td>
			</tr>
			<!--  
		<tr>
			<td>State</td>
			<td>
				<select name="state" size="1"  id="state" style="width:150px; border:1px solid #999999"  data-dojo-type="dijit/form/ComboBox"  >
					<option selected value="">State...</option>
					<option value="Kerala">Kerala</option>
					<option value="Gujrat">Gujrat</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>District</td>
			<td>
				<select name="state" size="1"  id="dist" style="width:150px; border:1px solid #999999"  data-dojo-type="dijit/form/ComboBox"  >
					<option selected value="">District...</option>
					<option value="Kasargod">Kasargod</option>
					<option value="Kannur">Kannur</option>
				</select>
			</td>				
		</tr>-->
		<tr>
			<td>Market Name</td>
			<td>
				<select name="marketname" size="1"  id="marketname" style="width:150px; border:1px solid #999999"  data-dojo-type="dijit/form/ComboBox"  >
				<%
					MarketDAO marketDAO = new MarketDAOImpl();
					MarketVO[] markets = marketDAO.getAllMarkets();
					for(int i=0; i < markets.length; i++)
					{
					%>
						<option value='<%=markets[i].getMarketId()%>'><%=markets[i].getName()%></option>
				
					<%
					}
					%>
				</select>
			</td>
		</tr>
		<tr>
			<td></td>
				<td><input type="submit" name="submit" value="Find" class="submit-button" /></td>
		</tr>
	</table>
</form>
<%=session.getAttribute("resultList")%>
<h1>Best price=
<%
Object result = session.getAttribute("result");
if(result != null)
{
	%>
		<%=(String)result%>
	<%
}
%>

</body>
</html>
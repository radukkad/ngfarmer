<%@page import="java.sql.ResultSet"%>
<%@page import="edu.sjsu.ngfarmer.dao.CommodityDAOImpl"%>
<%@page import="edu.sjsu.ngfarmer.dao.CommodityDAO"%>
<%@page import="edu.sjsu.ngfarmer.vo.CommodityVO"%>
<%@page import="edu.sjsu.ngfarmer.dao.MarketDAOImpl"%>
<%@page import="edu.sjsu.ngfarmer.dao.MarketDAO"%>
<%@page import="edu.sjsu.ngfarmer.vo.MarketVO"%>

<%@page import="java.util.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Next Generation Farmers </title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<link href="default.css" rel="stylesheet" type="text/css" />
<style type="text/css">
    table.gridtable {
      font-family: verdana,arial,sans-serif;
      font-size:11px;
      color:#333333;
      border-width: 1px;
      border-color: #666666;
      border-collapse: collapse;
    }
    table.gridtable th {
      border-width: 1px;
      padding: 8px;
      border-style: solid;
      border-color: #666666;
      background-color: #dedede;
    }
    table.gridtable td {
      border-width: 1px;
      padding: 8px;
      border-style: solid;
      border-color: #666666;
      background-color: #ffffff;
    }
</style>
    
<%

List outList = new ArrayList();
if( request.getAttribute("outList") != null)
{
	outList = (ArrayList)request.getAttribute("outList");
	

}

if(outList.size() > 0)
{
%>    
    
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Date', 'Sale Price'],
		<%
			Iterator itr = outList.iterator();		
          	while(itr.hasNext())
          	{
          		String[] resVal = (String[]) itr.next();
      			if(itr.hasNext())
      			{
      			
      				%>    
          				['<%=resVal[0]%>',  <%=resVal[1]%>],
         
  					<%
      			}
      			else
      			{
      				%>    
      				['<%=resVal[0]%>',  <%=resVal[1]%>]
     
					<%
  				
      			}
      	  	}
        %>  		  
      ]);

        var options = {
          title: 'Sales price trend ',
        	  curveType: 'function'
        };

        var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
    </script>

<%
}
%>
</head>
<body>

<div id="logo-wrap">
<%@include file="includes/topheader.jsp" %>
</div>
<%@include file="includes/header.jsp" %>
<!-- end header -->
<!-- start page -->
<div id="wrapper">
<div id="wrapper-btm">
<div id="page">
	<!-- start content -->
	<div id="content">
	
		<hr><br/>
	<h1>

		<%
		//req.setAttribute("outList", finalList);
	   // req.setAttribute("maxPriceDate", maxDate);
	    
		Object result = request.getAttribute("maxPrice");
		
		if(result != null && ((String)result).equalsIgnoreCase("Unavailable"))
		{
			out.println("Information not available") ;
				
		}
		else
		{
			if(result != null)
			{
			%>
				<%=(String)result%>
			<%
			}	
			%>
			&nbsp;
			(
			<%
			Object maxPriceDateStr = request.getAttribute("maxPriceDate");
			if(maxPriceDateStr != null )
			{
			%>
				<%=(String)maxPriceDateStr%>
			<%
			}
		%>
		
		)
		
		<%
		}
		%>
		
		</h1>
		<hr>
		
		
		</br></br>
		 <table border="1" class="gridtable" width="75%">
        
        <%
		if(null != request.getAttribute("commodityVO"))
        {
			CommodityVO commodity = (CommodityVO) request.getAttribute("commodityVO");
		%>
		  	<tr>
               <td> Commodity Name </td>
               <td> <%=commodity.getName()%> </td>
               </tr>
        
        <%
        }
        %>
        
         <%
		if(null != request.getAttribute("marketVO"))
        {
			MarketVO market = (MarketVO) request.getAttribute("marketVO");
		%>
		  	<tr>
               <td>  Market Name  </td>
               <td> <%=market.getName()%>
               <%
               
               if(market.getDistrict() != null && market.getDistrict() != "")
               {
            	   out.println(","+ market.getDistrict());
               }
               if(market.getState() != null && market.getState() != "")
               {
            	   out.println(","+ market.getState());
               }
               %>
                 </td>
               </tr>
        
        <%
        }
        %>
               <%-- <tr>
               <td> Best price sold  </td>
               <td> <%=maxPriceDateStr%> </td>
              </tr>
            
              <tr>
               <td> Week's best price  </td>
               <td> Rs. 44.00  </td>
              </tr>
 --%>              <%
              Object cellNumber = request.getAttribute("cellNumber");
            	if(cellNumber  != null && !((String)cellNumber).trim().equals("0"))  {
              %>
              <tr>
               <td> Contact  </td>
               <td> <%=cellNumber%> </td>
              </tr>
         	
         		<%
            	}
         		%>
         
         </table>
         <br></br>
		 <div id="chart_div" style="width: 464px; height: 250px;"></div>

	</div>
	<!-- end content -->
	<!-- start sidebar -->
	<%@include file="includes/sidebar.jsp" %>
	<!-- end sidebar -->
	<div style="clear: both;">&nbsp;</div>
	<br><br>
</div>
<!-- end page -->
</div>
</div>
<!-- start footer -->
<div id="footer">
	<%@include file="includes/footer.jsp" %>
</div>
<!-- end footer -->
<div align=center>This template  downloaded form <a href='http://all-free-download.com/free-website-templates/'>free website templates</a></div></body>
</html>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Next Generation Farmers </title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<link href="default.css" rel="stylesheet" type="text/css" />
<script>
<%

if( request.getAttribute("errorMsg") != null)
{
	%>
		alert('<%=request.getAttribute("errorMsg")%>');
	<%
}

%>

function validateForm()
{
var username =document.forms["LoginForm"]["cellno"].value;
if (username==null || username=="")
{
	alert("cell number must be filled out");
  	return false;
}
var password =document.forms["LoginForm"]["password"].value;
if (password==null || password=="")
{
	alert("Password must be filled out");
	return false;
}
}
</script>
</head>
<body>
<div id="logo-wrap">
<%@include file="includes/topheader.jsp" %>
</div>
<%@include file="includes/header.jsp" %>
<!-- end header -->
<!-- start page -->
<div id="wrapper">
<div id="wrapper-btm">
<div id="page">
	<!-- start content -->
	<div id="content">
		<div id="banner">&nbsp;</div>
		<div class="post">
			<h1 class="title">Welcome to Farmer's point</h1>
			<div class="entry">
				<p><img src="images/img06.jpg" alt="" width="140" height="125" class="left" />This is the place where farmers can freely collaborate with a 
				shared goal to get the best price for their commodities. We just capture the sales data from you and various other sources and process the information along with historcal data 
				to give you a best forecast on what the market price trend is. We help you to decide when and where to sell ..</br>Enjoy :)</p>
			</div>
			<div class="meta">
			<%--<p class="byline">Posted on July 21, 2007 byFreeCssTemplates</p>
				<p class="links"><a href="#" class="more">Read full article</a> <b>|</b> <a href="#" class="comments">Comments (32)</a></p>  --%>	
			</div>
		</div>
	</div>
	<!-- end content -->
	<!-- start sidebar -->
	<%@include file="includes/sidebar.jsp" %>
	<!-- end sidebar -->
	<div style="clear: both;">&nbsp;</div>
</div>
<!-- end page -->
</div>
</div>
<!-- start footer -->
<div id="footer">
	<%@include file="includes/footer.jsp" %>
</div>
<!-- end footer -->
<div align=center>This template is from <a href='http://all-free-download.com/free-website-templates/'>free website templates</a></div></body>
</html>

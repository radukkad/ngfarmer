<%@page import="java.text.SimpleDateFormat"%>
<%
Object farmerId = session.getAttribute("farmerId");
if(farmerId == null)
{
	request.getRequestDispatcher("/index1.jsp").forward(request, response);
}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@page import="edu.sjsu.ngfarmer.dao.CommodityDAOImpl"%>
<%@page import="edu.sjsu.ngfarmer.dao.CommodityDAO"%>
<%@page import="edu.sjsu.ngfarmer.vo.CommodityVO"%>
<%@page import="edu.sjsu.ngfarmer.dao.MarketDAOImpl"%>
<%@page import="edu.sjsu.ngfarmer.dao.MarketDAO"%>
<%@page import="edu.sjsu.ngfarmer.vo.MarketVO"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Next Generation Farmers </title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<link href="default.css" rel="stylesheet" type="text/css" />
<script>
<%

if( request.getAttribute("errorMsg") != null)
{
	%>
		alert('<%=request.getAttribute("errorMsg")%>');
	<%
}

%>
function validateForm()
{
var sellingPrice =document.forms["SaleEntryForm"]["sellingprice"].value;
if (sellingPrice==null || sellingPrice=="")
{
	alert("Selling price must be filled out");
  	return false;
}

}
</script>
</head>
<body>
<%

SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
String dateStr = formatter.format(new java.util.Date());

%>
<div id="logo-wrap">
<%@include file="includes/topheader.jsp" %>
</div>
<%@include file="includes/header.jsp" %>
<!-- end header -->
<!-- start page -->
<div id="wrapper">
<div id="wrapper-btm">
<div id="page">
	<!-- start content -->
	<div id="content">
		<div class="post">
			<h1 class="title">Sale Entry </h1>
			<form name="SaleEntryForm" action="saleentry" method="post"  onsubmit="return validateForm();">
				<input id="farmerId"  type="hidden" value=<%=farmerId%> name="farmerId" />
				<fieldset>
				 <table  border="0" cellspacing="3" width="60%">
				<tr><td>Commodity </td><td>
				
				<select name="commodity" size="1"  id="commodity" style="width:150px; border:1px solid #999999"  data-dojo-type="dijit/form/ComboBox"  >
				<%
					CommodityDAO commodityDAO = new CommodityDAOImpl();
					CommodityVO[] commodities = commodityDAO.getAllCommodities();
					for(int i=0; i < commodities.length; i++)
					{
				%>
			
					<option value='<%=commodities[i].getCommodityId()%>'><%=commodities[i].getName()%></option>
			
				<%
				}
				%>
				</select>
				
				</td>
				<tr><td>Market </td><td><select name="marketname" size="1"  id="marketname" style="width:150px; border:1px solid #999999"  data-dojo-type="dijit/form/ComboBox"  >
				<%
				MarketDAO marketDAO = new MarketDAOImpl();
				MarketVO[] markets = marketDAO.getAllMarkets();
				for(int i=0; i < markets.length; i++)
				{
				%>
					<option value='<%=markets[i].getMarketId()%>'><%=markets[i].getName()%></option>
			
				<%
				}
				%>
			</select>
			</td></tr>
				<tr><td>Sales Amt </td><td><input id="sellingprice"  type="text" size="15" maxlength="11" name="sellingprice" /></td></tr>
				<tr><td>Sales Date (mm/dd/yyyy) </td><td><input id="salesdate" value="<%=dateStr%>" type="text" size="15" maxlength="11" name="salesdate" /></td></tr>
				<tr><td></td><td><input type="submit" id="Login" value="Submit" /></td></tr>
				</table>
				</fieldset>
				
			</form>
		</div>
	</div>
	<!-- end content -->
	<!-- start sidebar -->
	<%-- <%@include file="includes/sidebar.jsp" %> --%>
	<!-- end sidebar -->
	<div style="clear: both;">&nbsp;</div>
</div>
<!-- end page -->
</div>
</div>
<!-- start footer -->
<div id="footer">
	<%@include file="includes/footer.jsp" %>
</div>
<!-- end footer -->
<div align=center>This template  downloaded form <a href='http://all-free-download.com/free-website-templates/'>free website templates</a></div></body>
</html>

<%@page import="edu.sjsu.ngfarmer.dao.CommodityDAOImpl"%>
<%@page import="edu.sjsu.ngfarmer.dao.CommodityDAO"%>
<%@page import="edu.sjsu.ngfarmer.vo.CommodityVO"%>
<%@page import="edu.sjsu.ngfarmer.dao.MarketDAOImpl"%>
<%@page import="edu.sjsu.ngfarmer.dao.MarketDAO"%>
<%@page import="edu.sjsu.ngfarmer.vo.MarketVO"%>

<div id="sidebar">
	<ul>
		<li id="login">
		<%
		
			String cellNo = null;
			int farmerId = 0;
			Object cellnoObj = session.getAttribute("cellno");
			Object farmerIdObj = session.getAttribute("farmerId");
			
			
			
			if(cellnoObj != null)
			{
				cellNo = (String)cellnoObj;
			}
			
			if(farmerIdObj != null)
			{
				farmerId = (Integer)farmerIdObj;
			}
			
			if(farmerIdObj != null)
			{		
			%>
			<h2>My Account</h2>
			<form name="LoginForm" action="login" method="post"  onsubmit="return validateForm();">
				<li><a href="myaccount.jsp">My Account</a></li>
				<li><a href="javascript:alert('Feature not yet implemented');">Preferences</a></li>
				<li><a href="saleentry.jsp">Enter Sales</a></li>
				<li><a href="logout">Logout</a></li>
			</form>
			
			<%
			}
			else
			{
			%>
			<h2>Login</h2>
			<form name="LoginForm" action="login" method="post"  onsubmit="return validateForm();">
				<fieldset>
				<input type="text" id="cellno" name="cellno" value="" />
				<input type="password" id="password" name="password" value="" /></br>
				<input type="submit" id="Login" value="Login" />
				</fieldset>
			</form>
			<a href="register.jsp">Register</a>
				
			<%
			}
			%>
		</li>
		<%
		if(farmerIdObj != null)
		{		
		%>
		<li>
			<h2>Price Checker</h2>
			
			<form action="FetchBestPrice" method="post">
				<fieldset>
				<select name="commodity" size="1"  id="commodity" style="width:150px; border:1px solid #999999"  data-dojo-type="dijit/form/ComboBox"  >
				<option selected value="">Commodity...</option>
				<%
					CommodityDAO commodityDAO = new CommodityDAOImpl();
					CommodityVO[] commodities = commodityDAO.getAllCommodities();
					for(int i=0; i < commodities.length; i++)
					{
				%>
			
					<option value='<%=commodities[i].getCommodityId()%>'><%=commodities[i].getName()%></option>
			
				<%
				}
				%>
				</select>
			
				<select name="marketname" size="1"  id="marketname" style="width:150px; border:1px solid #999999"  data-dojo-type="dijit/form/ComboBox"  >
				<option selected value="">Market...</option>
				<%
				MarketDAO marketDAO = new MarketDAOImpl();
				MarketVO[] markets = marketDAO.getAllMarkets();
				for(int i=0; i < markets.length; i++)
				{
				%>
					<option value='<%=markets[i].getMarketId()%>'><%=markets[i].getName()%></option>
			
				<%
				}
				%>
			</select>
				</br>
				</fieldset>
				<input type="submit" id="Check" value="Check" />
			
			</form>
			
			<%--
			<ul>
				<li><a href="#">Check best price</a></li>
				<li><a href="#">Magna lacus bibendum mauris</a></li>
				<li><a href="#">Nec metus sed donec</a></li>
				<li><a href="#">Magna lacus bibendum mauris</a></li>
				<li><a href="#">Velit semper nisi molestie</a></li>
				<li><a href="#">Eget tempor eget nonummy</a></li> 
			</ul>--%>
		</li>
		<%
		}
		%>
	</ul>
</div>
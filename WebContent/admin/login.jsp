<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Next Generation Farmers </title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<link href="../default.css" rel="stylesheet" type="text/css" />
<script>
<%
if( request.getAttribute("errorMsg") != null)
{
	%>
		alert('<%=request.getAttribute("errorMsg")%>');
	<%
}

%>
function validateForm()
{
var username =document.forms["LogonForm"]["userId"].value;
if (username==null || username=="")
{
	alert("UserId must be filled out");
  	return false;
}
var password =document.forms["LogonForm"]["password"].value;
if (password==null || password=="")
{
	alert("Password must be filled out");
	return false;
}

}
</script>
</head>
<body>
<div id="logo-wrap">
<%@include file="includes/topheader.jsp" %>
</div>
<%@include file="includes/header.jsp" %>
<!-- end header -->
<!-- start page -->
<div id="wrapper">
<div id="wrapper-btm">
<div id="page">
	<!-- start content -->
	<div id="content">
		<div class="post">
			<h1 class="title">Login </h1>
			<form name="RegisterForm" action="adminlogin" method="post"  onsubmit="return validateForm();">
				<fieldset>
				 <table  border="0" cellspacing="3" width="50%">
				<tr><td>User Id</td><td><input type="text" id="userId" name="userId" value="" /></td>
				<tr><td>Password </td><td><input type="password" id="password" name="password" value="" /></td></tr>
				<tr><td></td><td><input type="submit" id="Login" value="Login" /></td></tr>
				</table>
				</fieldset>
				
			</form>
		</div>
	</div>
	<!-- end content -->
	<!-- start sidebar -->
	<%-- <%@include file="includes/sidebar.jsp" %> --%>
	<!-- end sidebar -->
	<div style="clear: both;">&nbsp;</div>
</div>
<!-- end page -->
</div>
</div>
<!-- start footer -->
<div id="footer">
	<%@include file="includes/footer.jsp" %>
</div>
<!-- end footer -->
<div align=center>This template  downloaded form <a href='http://all-free-download.com/free-website-templates/'>free website templates</a></div></body>
</html>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Next Generation Farmers </title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<link href="../default.css" rel="stylesheet" type="text/css" />
<script>
<%

Object isadmin = session.getAttribute("admin");
if(isadmin == null)
{
	request.getRequestDispatcher("login.jsp").forward(request, response);
}

if( request.getAttribute("errorMsg") != null)
{
	%>
		alert('<%=request.getAttribute("errorMsg")%>');
	<%
}

%>
function validateForm()
{
var commodityname =document.forms["AddMarketForm"]["marketname"].value;
if (commodityname==null || commodityname=="")
{
	alert("market name must be filled out");
  	return false;
}
}
</script>
</head>
<body>
<div id="logo-wrap">
<%@include file="includes/topheader.jsp" %>
</div>
<%@include file="includes/header.jsp" %>
<!-- end header -->
<!-- start page -->
<div id="wrapper">
<div id="wrapper-btm">
<div id="page">
	<!-- start content -->
	<div id="content">
		<div class="post">
			<h1 class="title">Add new market </h1>
			<form name="AddMarketForm" action="addmarket" method="post"  onsubmit="return validateForm();">
				<fieldset>
				 <table  border="0" cellspacing="3" width="50%">
				<tr><td>Market Name </td><td><input type="text" id="marketname" name="marketname" value="" /></td>
				<tr><td>District  </td><td><input type="text" id="dist" name="dist" value="" /></td></tr>
				<tr><td>Postal Code  </td><td><input type="text" id="postalcode" name="postalcode" value="" /></td></tr>
				<tr><td>State  </td><td><input type="text" id="state" name="state" value="" /></td></tr>
				<tr><td>Country  </td><td><input type="text" id="country" name="country" value="" /></td></tr>
				<tr><td></td><td><input type="submit" id="Add" value="Add" /></td></tr>
				</table>
				</fieldset>
				
			</form>
		</div>
	</div>
	<!-- end content -->
	<!-- start sidebar -->
	<%@include file="includes/sidebar.jsp" %>
	<!-- end sidebar -->
	<div style="clear: both;">&nbsp;</div>
</div>
<!-- end page -->
</div>
</div>
<!-- start footer -->
<div id="footer">
	<%@include file="includes/footer.jsp" %>
</div>
<!-- end footer -->
<div align=center>This template  downloaded form <a href='http://all-free-download.com/free-website-templates/'>free website templates</a></div></body>
</html>

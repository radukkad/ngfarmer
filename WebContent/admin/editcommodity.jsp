<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@page import="edu.sjsu.ngfarmer.dao.CommodityDAOImpl"%>
<%@page import="edu.sjsu.ngfarmer.dao.CommodityDAO"%>
<%@page import="edu.sjsu.ngfarmer.vo.CommodityVO"%>
<%
Object isadmin = session.getAttribute("admin");
if(isadmin == null)
{
	request.getRequestDispatcher("login.jsp").forward(request, response);
}
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Next Generation Farmers </title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<link href="../default.css" rel="stylesheet" type="text/css" />
<script>
<%

if( request.getAttribute("errorMsg") != null)
{
	%>
		alert('<%=request.getAttribute("errorMsg")%>');
	<%
}

%>
function validateForm()
{
var commodityname =document.forms["UpdCommodityForm"]["commodityname"].value;
if (commodityname==null || commodityname=="")
{
	alert("commodity name must be filled out");
  	return false;
}
}
</script>
</head>
<body>
<%
int commodityId = 0;
String commodityIdStr = request.getParameter("commodityId");
if(commodityIdStr == null && request.getAttribute("commodityId") != null)
{
	commodityIdStr = (String)request.getAttribute("commodityId");
}
if(commodityIdStr == null)
{
	request.getRequestDispatcher("/commoditylist.jsp").forward(request, response);
}
else
{
	commodityId = Integer.parseInt(commodityIdStr);
}

CommodityDAO commodityDAO = new CommodityDAOImpl();
CommodityVO commodityVO = commodityDAO.getCommodityById(commodityId);

%>
<div id="logo-wrap">
<%@include file="includes/topheader.jsp" %>
</div>
<%@include file="includes/header.jsp" %>
<!-- end header -->
<!-- start page -->
<div id="wrapper">
<div id="wrapper-btm">
<div id="page">
	<!-- start content -->
	<div id="content">
		<div class="post">
			<h1 class="title">Edit commodity </h1>
			<form name=UpdCommodityForm" action="updcommodity" method="post"  onsubmit="return validateForm();">
				<input type="hidden" id="commodityId" name="commodityId" value="<%=commodityVO.getCommodityId() %>" />
				<fieldset>
				 <table  border="0" cellspacing="3" width="50%">
				<tr><td>Commodity Name </td><td><input type="text" id="commodityname" name="commodityname" value="<%=commodityVO.getName() %>" /></td>
				<tr><td>Description </td><td><input type="text" id="desc" name="desc" value="<%
				
				String descStr =commodityVO.getDesc();
				if(descStr != null && descStr != "")
				{
					%>
						<%=descStr.trim()%>
					<%
				}
				
				%>" /></td></tr>
				
				<tr><td></td><td><input type="submit" id="Update" value="Update" /></td></tr>
				</table>
				</fieldset>
				
			</form>
		</div>
	</div>
	<!-- end content -->
	<!-- start sidebar -->
	<%@include file="includes/sidebar.jsp" %> 
	<!-- end sidebar -->
	<div style="clear: both;">&nbsp;</div>
</div>
<!-- end page -->
</div>
</div>
<!-- start footer -->
<div id="footer">
	<%@include file="includes/footer.jsp" %>
</div>
<!-- end footer -->
<div align=center>This template  downloaded form <a href='http://all-free-download.com/free-website-templates/'>free website templates</a></div></body>
</html>

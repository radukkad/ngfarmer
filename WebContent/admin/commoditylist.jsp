<%@page import="edu.sjsu.ngfarmer.dao.CommodityDAOImpl"%>
<%@page import="edu.sjsu.ngfarmer.dao.CommodityDAO"%>
<%@page import="edu.sjsu.ngfarmer.vo.CommodityVO"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%
Object isadmin = session.getAttribute("admin");
if(isadmin == null)
{
	request.getRequestDispatcher("login.jsp").forward(request, response);
}
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Next Generation Farmers </title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<link href="../default.css" rel="stylesheet" type="text/css" />
<style>
table.gridtable {
      font-family: verdana,arial,sans-serif;
      font-size:11px;
      color:#333333;
      border-width: 1px;
      border-color: #666666;
      border-collapse: collapse;
    }
    table.gridtable th {
      border-width: 1px;
      padding: 8px;
      border-style: solid;
      border-color: #666666;
      background-color: #dedede;
    }
    table.gridtable td {
      border-width: 1px;
      padding: 8px;
      border-style: solid;
      border-color: #666666;
      background-color: #ffffff;
    }
    table.fixed { table-layout:fixed; }
	table.fixed td { overflow: hidden; }
</style>
</head>
<body>
<div id="logo-wrap">
<%@include file="includes/topheader.jsp" %>
</div>
<%@include file="includes/header.jsp" %>
<!-- end header -->
<!-- start page -->
<div id="wrapper">
<div id="wrapper-btm">
<div id="page">
	<!-- start content -->
	<div id="content">
		<div class="post">
			<h1 class="title">List of commodities </h1>
	
			<table border="1" class="gridtable" width="100%">
              <tr bgcolor="#00FF00">
                <th>Commodity Id</th>
                <th>Name</th>
                <th>Description</th>
                <th></th>
              </tr>
              
              	<%
					CommodityDAO commodityDAO = new CommodityDAOImpl();
					CommodityVO[] commodities = commodityDAO.getAllCommodities();
					for(int i=0; i < commodities.length; i++)
					{
				%>
			
			              <tr>
            				  <td><%=commodities[i].getCommodityId()%></td>
              				  <td><%=commodities[i].getName()%></td>
             				  <td><% 
             				  String val = commodities[i].getDesc();
             				  if(val != null && val != "")
             				  {
								%>
									<%=val%>
								<%

             				  }
             				  
             				  %></td>
			            	  <td>	<a href="editcommodity.jsp?commodityId=<%=commodities[i].getCommodityId()%>">Edit</a> </td>
			             </tr>
				<%
				}
				%>
				
		</table>
		</div>
	</div>
	<!-- end content -->
	<!-- start sidebar -->
	<%@include file="includes/sidebar.jsp" %>
	<!-- end sidebar -->
	<div style="clear: both;">&nbsp;</div>
</div>
<!-- end page -->
</div>
</div>
<!-- start footer -->
<div id="footer">
	<%@include file="includes/footer.jsp" %>
</div>
<!-- end footer -->
<div align=center>This template  downloaded form <a href='http://all-free-download.com/free-website-templates/'>free website templates</a></div></body>
</html>

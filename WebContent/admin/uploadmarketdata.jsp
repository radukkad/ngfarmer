<%@page import="sun.misc.BASE64Encoder"%>
<%@page import="javax.crypto.Mac"%>
<%@page import="java.net.URL"%>
<%@page import="javax.crypto.spec.SecretKeySpec"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%
Object isadmin = session.getAttribute("admin");
if(isadmin == null)
{
	request.getRequestDispatcher("login.jsp").forward(request, response);
}
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Next Generation Farmers </title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<link href="../default.css" rel="stylesheet" type="text/css" />
<script>
<%

if( request.getParameter("success") != null)
{
	%>
		alert('Market data file uploaded successfully');
	<%
}

%>
function validateForm()
{
var bucket =document.forms["UploadMarketDataForm"]["bucket"].value;
if (bucket==null || bucket=="")
{
	alert("Bucket name must be filled out");
  	return false;
}
var datafile =document.forms["UploadMarketDataForm"]["datafile"].value;
if (datafile==null || datafile=="")
{
	alert("Datafile name must be selected");
  	return false;
}
}
</script>
</head>
<body>
<%
String aws_secret_key = "EuE20dipOE/3FvJ1Vk6OJ7JEMKYqRiH4hQMvbpVU";
String policy_document = "{'expiration': '2019-01-01T00:00:00Z',"
	+"'conditions': [  " + 
	" {'bucket': 'farmerspoint'},  " +
		"['starts-with', '$key', 'input/'], " + 
	    "{'acl': 'private'},  " + 
	    " {'success_action_redirect': 'http://ngfarmer-dev-fwte4pjeh2.elasticbeanstalk.com/admin/uploadmarketdata.jsp?success=true'}, " +
	    " ['starts-with', '$Content-Type', ''], " + 
	    "   ['content-length-range', 0, 1048576] " +
	 	" ] "+
	 "}";
	 
String policy = (new BASE64Encoder()).encode(
			    policy_document.getBytes("UTF-8")).replaceAll("\n","").replaceAll("\r","");

Mac hmac = Mac.getInstance("HmacSHA1");
hmac.init(new SecretKeySpec(aws_secret_key.getBytes("UTF-8"), "HmacSHA1"));

String signature = (new BASE64Encoder()).encode(
			    hmac.doFinal(policy.getBytes("UTF-8")))
			    .replaceAll("\n", "");

%>
<div id="logo-wrap">
<%@include file="includes/topheader.jsp" %>
</div>
<%@include file="includes/header.jsp" %>
<!-- end header -->
<!-- start page -->
<div id="wrapper">
<div id="wrapper-btm">
<div id="page">
	<!-- start content -->
	<div id="content">
		<div class="post">
			<h1 class="title">Upload a market data </h1>
			<fieldset>
			<form action="https://farmerspoint.s3.amazonaws.com/" method="post" enctype="multipart/form-data">
      			<input type="hidden" name="key" value="input/farmers-market-data.txt">
      			<input type="hidden" name="AWSAccessKeyId" value="AKIAII677KWPCHCI2ZVA"> 
      			<input type="hidden" name="acl" value="private"> 
      			<input type="hidden" name="success_action_redirect" value="http://ngfarmer-dev-fwte4pjeh2.elasticbeanstalk.com/admin/uploadmarketdata.jsp?success=true">
      			<input type="hidden" name="policy" value="<%=policy%>">
      			<input type="hidden" name="signature" value="<%=signature%>">
      			<input type="hidden" name="Content-Type" value="text/plain">
      		    <table  border="0" cellspacing="3" width="70%">
				<tr><td>Select file  :</td><td><input name="file" type="file">  </td>
				<tr><td></td><td><input type="submit" value="Upload File"> </td></tr>
				</table>
      			<br> 
      			
    		</form>
    		</fieldset>
		</div>
	</div>
	<!-- end content -->
	<!-- start sidebar -->
	<%@include file="includes/sidebar.jsp" %>
	<!-- end sidebar -->
	<div style="clear: both;">&nbsp;</div>
</div>
<!-- end page -->
</div>
</div>
<!-- start footer -->
<div id="footer">
	<%@include file="includes/footer.jsp" %>
</div>
<!-- end footer -->
<div align=center>This template  downloaded form <a href='http://all-free-download.com/free-website-templates/'>free website templates</a></div></body>
</html>

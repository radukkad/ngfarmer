<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<%@page import="edu.sjsu.ngfarmer.dao.FarmerDAOImpl"%>
<%@page import="edu.sjsu.ngfarmer.dao.FarmerDAO"%>
<%@page import="edu.sjsu.ngfarmer.vo.FarmerVO"%>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Next Generation Farmers </title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<link href="default.css" rel="stylesheet" type="text/css" />
<script>
<%

int farmerId = 0;
Object farmerIdObj = session.getAttribute("farmerId");
if(farmerIdObj == null)
{
	request.getRequestDispatcher("/login.jsp").forward(request, response);
}
else
{
	farmerId = ((Integer)farmerIdObj).intValue();
}

FarmerDAO farmerDAO = new FarmerDAOImpl();
FarmerVO farmerVO = farmerDAO.getFarmerByFarmerId(farmerId);

if( request.getAttribute("errorMsg") != null)
{
	%>
		alert('<%=request.getAttribute("errorMsg")%>');
	<%
}

%>
function validateForm()
{
var username =document.forms["UpdateForm"]["cellnumber"].value;
if (username==null || username=="")
{
	alert("cell number must be filled out");
  	return false;
}
var password =document.forms["UpdateForm"]["pwd"].value;
if (password==null || password=="")
{
	alert("Password must be filled out");
	return false;
}

var password1 =document.forms["UpdateForm"]["pwd1"].value;
if (password1 != password)
{
	alert("Passwords doesn't match");
	return false;
}



}
</script>
</head>
<body>
<div id="logo-wrap">
<%@include file="includes/topheader.jsp" %>
</div>
<%@include file="includes/header.jsp" %>
<!-- end header -->
<!-- start page -->
<div id="wrapper">
<div id="wrapper-btm">
<div id="page">
	<!-- start content -->
	<div id="content">
		<div class="post">
			<h1 class="title">My Account </h1>
			<form name="UpdateForm" action="update" method="post"  onsubmit="return validateForm();">
				<input type="hidden" id="farmerId" name="farmerId" value="<%=farmerVO.getFarmerId()%>" />
				<fieldset>
				 <table  border="0" cellspacing="3" width="50%">
				<tr><td>Cell Number </td><td><input type="text" id="cellnumber" name="cellnumber" value="<%=farmerVO.getCellNo()%>" /></td>
				<tr><td>Password </td><td><input type="password" id="pwd" name="pwd" value="<%=farmerVO.getPassword()%>" /></td></tr>
				<tr><td>Re-enter password </td><td><input type="password" id="pwd1" name="pwd1" value="" /></td></tr>
				<tr><td></td><td><input type="submit" id="Login" value="Update" /></td></tr>
				</table>
				</fieldset>
				
			</form>
		</div>
	</div>
	<!-- end content -->
	<!-- start sidebar -->
	<%-- <%@include file="includes/sidebar.jsp" %> --%>
	<!-- end sidebar -->
	<div style="clear: both;">&nbsp;</div>
</div>
<!-- end page -->
</div>
</div>
<!-- start footer -->
<div id="footer">
	<%@include file="includes/footer.jsp" %>
</div>
<!-- end footer -->
<div align=center>This template  downloaded form <a href='http://all-free-download.com/free-website-templates/'>free website templates</a></div></body>
</html>
